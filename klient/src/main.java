

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.Timer;

public class main {

	
	
	private JFrame frame;
	private JTextField KlientImie;
	private JTextField KlientNazwisko;
	private JTextField KlientPesel;
	private JTextField AutoMarka;
	private JTextField AutoModel;
	private JTextField AutoRej;
	
	private DefaultListModel<String> AutaDefault=new DefaultListModel<String>();
	private DefaultListModel<String> KlienciDefault=new DefaultListModel<String>();
	private DefaultListModel<String> WypozyczeniaDefault=new DefaultListModel<String>();
	/**
	 * @wbp.nonvisual location=221,19
	 */
	private final Timer timer = new Timer(1000, (ActionListener) null);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}
	
	private void polaczenie(){
		
		
		Socket sc;
		try {
			sc = new Socket("localhost", 60606);
			OutputStream os = sc.getOutputStream();
			PrintWriter wrt = new PrintWriter(os);
			wrt.println("samochody:get:");
			wrt.flush();
			InputStream is = sc.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);
			Object[] obj = (Object[])ois.readObject();
			AutaDefault.clear();
			for(Object o:obj)
			{
				AutaDefault.addElement(o.toString());;
			}				
			sc.close();
			
			sc = new Socket("localhost", 60606);
			os = sc.getOutputStream();
			wrt = new PrintWriter(os);
			wrt.println("klienci:get:");
			wrt.flush();
			is = sc.getInputStream();
			ois = new ObjectInputStream(is);
			obj = (Object[])ois.readObject();
			KlienciDefault.clear();
			for(Object o:obj)
			{
				KlienciDefault.addElement(o.toString());;
			}				
			sc.close();
			
			sc = new Socket("localhost", 60606);
			os = sc.getOutputStream();
			wrt = new PrintWriter(os);
			wrt.println("wypozyczenia:get:");
			wrt.flush();
			is = sc.getInputStream();
			ois = new ObjectInputStream(is);
			obj = (Object[])ois.readObject();
			WypozyczeniaDefault.clear();
			for(Object o:obj)
			{
				WypozyczeniaDefault.addElement(o.toString());;
			}				
			sc.close();
			
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		
		
		
		
		/*wrt.print("samochody:wypozycz:Volkswagen;Golf;czerwony;KR231RF;");
		wrt.print("samochody:wypozycz:Opel;Vectra;czarny;KR121ED;");
		wrt.print("samochody:wypozycz:Ford;Mondeo;bia�y;DW541RF;");
		wrt.print("samochody:wypozycz:BMW;E50;niebieski;GDA321ZF;");
		wrt.print("samochody:zwroc:Volkswagen;Passat;czerwony;KR231RF;");
		wrt.print("samochody:zwroc:Opel;Agila;czarny;PZ131RF;;");
		wrt.print("samochody:zwroc:Ford;Fiesta;niebieski;KR231CF;");
		wrt.print("samochody:zwroc:BMW;E70;czarny;PZ134CV;");
		*/
		
		}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 585, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 549, 205);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Dodawanie", null, panel, null);
		panel.setLayout(null);
		
		JButton btnDodaj = new JButton("dodaj samoch\u00F3d");
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Socket sc;
				try {
					sc = new Socket("localhost", 60606);
					OutputStream os = sc.getOutputStream();
					PrintWriter wrt = new PrintWriter(os);
					wrt.println("samochody:dodaj:"+AutoMarka.getText()+";"+AutoModel.getText()+";"+AutoRej.getText()+";");
					wrt.flush();
					sc.close();
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnDodaj.setBounds(10, 143, 153, 23);
		panel.add(btnDodaj);
		
		JButton btnNewButton = new JButton("dodaj klienta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Socket sc;
				try {
					sc = new Socket("localhost", 60606);
					OutputStream os = sc.getOutputStream();
					PrintWriter wrt = new PrintWriter(os);
					wrt.println("klienci:dodaj:"+KlientImie.getText()+";"+KlientNazwisko.getText()+";"+KlientPesel.getText()+";");
					wrt.flush();
					sc.close();
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(10, 69, 153, 23);
		panel.add(btnNewButton);
		
		KlientImie = new JTextField();
		KlientImie.setBounds(10, 36, 86, 20);
		panel.add(KlientImie);
		KlientImie.setColumns(10);
		
		JLabel lblImie = new JLabel("Imie");
		lblImie.setBounds(10, 11, 46, 14);
		panel.add(lblImie);
		
		KlientNazwisko = new JTextField();
		KlientNazwisko.setColumns(10);
		KlientNazwisko.setBounds(106, 36, 86, 20);
		panel.add(KlientNazwisko);
		
		KlientPesel = new JTextField();
		KlientPesel.setColumns(10);
		KlientPesel.setBounds(202, 36, 86, 20);
		panel.add(KlientPesel);
		
		JLabel lblNazwisko = new JLabel("Nazwisko");
		lblNazwisko.setBounds(106, 11, 46, 14);
		panel.add(lblNazwisko);
		
		JLabel lblPesel = new JLabel("Pesel");
		lblPesel.setBounds(202, 11, 46, 14);
		panel.add(lblPesel);
		
		AutoMarka = new JTextField();
		AutoMarka.setBounds(10, 119, 86, 20);
		panel.add(AutoMarka);
		AutoMarka.setColumns(10);
		
		AutoModel = new JTextField();
		AutoModel.setColumns(10);
		AutoModel.setBounds(106, 119, 86, 20);
		panel.add(AutoModel);
		
		AutoRej = new JTextField();
		AutoRej.setColumns(10);
		AutoRej.setBounds(202, 119, 86, 20);
		panel.add(AutoRej);
		
		JLabel lblMarka = new JLabel("Marka");
		lblMarka.setBounds(10, 104, 46, 14);
		panel.add(lblMarka);
		
		JLabel lblModel = new JLabel("Model");
		lblModel.setBounds(106, 103, 46, 14);
		panel.add(lblModel);
		
		JLabel lblNumerRej = new JLabel("Numer Rej.");
		lblNumerRej.setBounds(202, 104, 70, 14);
		panel.add(lblNumerRej);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Wypo�yczanie", null, panel_1, null);
		panel_1.setLayout(null);
		
		JList Auta = new JList(AutaDefault);
		Auta.setBounds(286, 0, 258, 132);
		panel_1.add(Auta);
		
		JList Klienci = new JList(KlienciDefault);
		Klienci.setBounds(0, 0, 276, 132);
		panel_1.add(Klienci);
		
		JButton btnZwroc = new JButton("wypo\u017Cycz samoch\u00F3d");
		btnZwroc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Socket sc;
				try {
					sc = new Socket("localhost", 60606);
					OutputStream os = sc.getOutputStream();
					PrintWriter wrt = new PrintWriter(os);
					wrt.println("wypozyczenia:add:"+Klienci.getSelectedIndex()+";"+Auta.getSelectedIndex()+";");
					wrt.flush();
					sc.close();
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnZwroc.setBounds(10, 143, 162, 23);
		panel_1.add(btnZwroc);
		

		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Zwracanie", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnWypozycz = new JButton("zwr\u00F3\u0107 samoch\u00F3d");
		btnWypozycz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnWypozycz.setBounds(10, 143, 131, 23);
		panel_2.add(btnWypozycz);
		
		JList Wypozycenia = new JList(WypozyczeniaDefault);
		Wypozycenia.setBounds(0, 0, 544, 137);
		panel_2.add(Wypozycenia);
		
		timer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Auta.repaint();
				Klienci.repaint();
				Wypozycenia.repaint();
			}
		});
		
		JButton btnPobierz = new JButton("pobierz ");
		btnPobierz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				polaczenie();
			}
		});
		btnPobierz.setBounds(20, 227, 89, 23);
		frame.getContentPane().add(btnPobierz);
		
	
	}
}
