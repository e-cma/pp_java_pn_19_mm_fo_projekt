import java.awt.EventQueue;
import java.awt.Frame;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ListModel;
import javax.swing.Timer;

import java.io.*;
import java.net.*;
import java.util.Arrays;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;

import javax.swing.JList;

import java.awt.Cursor;

public class main {

	private JFrame frame;
	/**
	 * @wbp.nonvisual location=51,19
	 */
	private final Timer timer = new Timer(1000, (ActionListener) null);
	/**
	 * @wbp.nonvisual location=173,19
	 */

	private ServerSocket serverSocket;// = new ServerSocket(5555);
	private Socket client;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	private DefaultListModel<String> ListAutaModel = new DefaultListModel<String>();
	private JList ListAuta;
	private DefaultListModel<String> ListKlienciModel = new DefaultListModel<String>();
	private JList ListKlienci;
	private DefaultListModel<String> ListWypozyczeniaModel = new DefaultListModel<String>();
	private JList ListWypozyczenia;
	private JButton btnPolocz;

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		timer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				timeraction();
			}
		});
		timer.setDelay(250);
		



		frame = new JFrame();
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 37, 434, 225);
		frame.getContentPane().add(tabbedPane);

		ListAuta = new JList(ListAutaModel);
		tabbedPane.addTab("Auta", null, ListAuta, null);

		ListKlienci = new JList(ListKlienciModel);
		tabbedPane.addTab("Klienci", null, ListKlienci, null);

		ListWypozyczenia = new JList(ListWypozyczeniaModel);
		tabbedPane.addTab("Wypozyczenia", null, ListWypozyczenia, null);

		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 434, 26);
		frame.getContentPane().add(toolBar);

		JButton btnWczytaj = new JButton("Wczytaj");
		btnWczytaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				XMLDecoder Decoder;
				try {
					Object[] Objectarray;
					Decoder = new XMLDecoder(new BufferedInputStream(
							new FileInputStream("Klienci.xml")));
					Objectarray=(Object[]) Decoder.readObject();
					ListKlienciModel.clear();
					for(Object o:Objectarray)
					{
						ListKlienciModel.addElement(o.toString());
					}
					//ListKlienciModel.copyInto((String[]) Decoder.readObject());
					Decoder.close();
					Decoder = new XMLDecoder(new BufferedInputStream(
							new FileInputStream("Auta.xml")));
					Objectarray=(Object[]) Decoder.readObject();
					ListAutaModel.clear();
					for(Object o:Objectarray)
					{
						ListAutaModel.addElement(o.toString());
					}
					//ListAutaModel.copyInto((String[]) Decoder.readObject());
					Decoder.close();
					Decoder = new XMLDecoder(new BufferedInputStream(
							new FileInputStream("Wypozyczenia.xml")));
					Objectarray=(Object[]) Decoder.readObject();
					ListWypozyczeniaModel.clear();
					for(Object o:Objectarray)
					{
						ListWypozyczeniaModel.addElement(o.toString());;
					}
					//ListWypozyczeniaModel.copyInto((String[]) Decoder.readObject());
					Decoder.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		toolBar.add(btnWczytaj);

		JButton btnZapisz = new JButton("Zapisz");
		btnZapisz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//ListKlienciModel.addElement("TEST");
				try {
					XMLEncoder Encoder = new XMLEncoder(
							new BufferedOutputStream(new FileOutputStream(
									"Klienci.xml")));
					Encoder.writeObject(ListKlienciModel.toArray());
					Encoder.close();
					Encoder = new XMLEncoder(new BufferedOutputStream(
							new FileOutputStream("Auta.xml")));
					Encoder.writeObject(ListAutaModel.toArray());
					Encoder.close();
					Encoder = new XMLEncoder(new BufferedOutputStream(
							new FileOutputStream("Wypozyczenia.xml")));
					Encoder.writeObject(ListWypozyczeniaModel.toArray());
					Encoder.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		toolBar.add(btnZapisz);
		
		btnPolocz = new JButton("Polocz");
		btnPolocz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
						//System.out.print("czekam");
						ListAuta.repaint();
						ListKlienci.repaint();
						ListWypozyczenia.repaint();
						// serverSocket.setSoTimeout(250);
						/*try {
							serverSocket = new ServerSocket(60606);
							client = serverSocket.accept();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}*/
						//System.out.print("nie czekam");
						
/*	serverSocket.setSoTimeout(100);
						client.setSoTimeout(100);*/
						timer.start();
						
			}
		});
		toolBar.add(btnPolocz);
		

		
	}

	private void timeraction() {
		// System.out.println("OK");
		String tmp;
		String delims = "[:]";
		String[] tokens;
		//System.out.println("ok");
		InputStream is;
		try {
			serverSocket = new ServerSocket(60606);
			//serverSocket.setSoTimeout(250);
			client = serverSocket.accept();
			//client.setSoTimeout(250);
			//System.out.print("nie czekam");
			

			
			is = client.getInputStream();

			InputStreamReader isr = new InputStreamReader(is);
			
			BufferedReader rdr = new BufferedReader(isr);
			tmp = rdr.readLine();
			System.out.println(tmp);
			tokens = tmp.split(delims);

			switch (tokens[0]) {
			case "klienci":
				switch (tokens[1]) {
				case "get":
					SendListModel(ListKlienciModel);
					break;
				case "dodaj":

					String przerwy = "[;]";
					String[] elementy;
					elementy = tokens[2].split(przerwy);
					// ///////////////////////////////
					ListKlienciModel.addElement(elementy[0] + " " + elementy[1]
							+ " " + elementy[2]);

					break;
				case "delete":
					String przerwy2 = "[;]";
					String[] elementy2;
					elementy2 = tokens[2].split(przerwy2);
					ListKlienciModel.remove(Integer.parseInt(elementy2[0]));
					break;
				}
				break;
			case "samochody":
				switch (tokens[1]) {
				case "get":
					SendListModel(ListAutaModel);
					break;
				case "dodaj":

					String przerwy = "[;]";
					String[] elementy;
					elementy = tokens[2].split(przerwy);
					// ///////////////////////////////
					ListAutaModel.addElement(elementy[0] + " " + elementy[1]
							+ " " + elementy[2]);
					frame.repaint();
					break;
				case "delete":
					String przerwy2 = "[;]";
					String[] elementy2;
					elementy2 = tokens[2].split(przerwy2);
					ListAutaModel.remove(Integer.parseInt(elementy2[0]));
					break;
				}
				break;

			case "wypozyczenia":
				switch (tokens[1]) {
				case "get":
					SendListModel(ListWypozyczeniaModel);
					break;
				case "add":

					String przerwy = "[;]";
					String[] elementy;
					elementy = tokens[2].split(przerwy);
					// ///////////////////////////////
					ListWypozyczeniaModel.addElement(ListKlienciModel
							.elementAt(Integer.parseInt(elementy[0]))
							+ " "
							+ ListAutaModel.elementAt(Integer
									.parseInt(elementy[1])));

					break;
				case "delete":
					String przerwy2 = "[;]";
					String[] elementy2;
					elementy2 = tokens[2].split(przerwy2);
					ListWypozyczeniaModel
							.remove(Integer.parseInt(elementy2[0]));
					break;
				}
				break;

			}
			serverSocket.close();
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}

	}

	void SendListModel(DefaultListModel model) {
		
		try {
			OutputStream os;
			os = client.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(model.toArray());
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
